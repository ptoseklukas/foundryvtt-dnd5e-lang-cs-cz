# FoundryVTT D&D5e module lang cs-CZ - BETA

**Aktuální stav překladu: 63% (aktualizoval jsem na poslední verzi DnD modulu kde přibylo pár řetezců navíc, tudíž stav klesl) - aktualizace 20. 1. 2022**

**Aktuální stav korektur a kontrol: 0%**

**Pokud by chtěl někdo překlad dokopat, napište mi na Discord **Mrkew#3758 nebo email níže****


## Česky

Tenhle modul umožní zvolit *Češtinu* ve [FoundryVTT](http://foundryvtt.com/ "Foundry Virtual Tabletop") D&D5e modulu.
Obsahuje nekompletní překlad D&D5e modulu. 

Pokud máš nějaké doporučení a nebo si našel nějaké chyby v překladu, napiš mi nejlépe na discord **Mrkew#3758** nebo na můj e-mail mrkew2(zavináč)gmail.com. Vždy se ozvu, takže kdybych se neozval napiš mi znovu. 

### Instalace

V hlavním menu kliknout na 'Add-On Modules' a poté kliknout 'Install Module' a dole do 'Manifest URL' vložit adresu níže. 

https://gitlab.com/ptoseklukas/foundryvtt-dnd5e-lang-cs-cz/-/raw/master/cs-CZ/module.json

Pokud to nebude fungovat zkus stáhnout soubor [cs-CZ.zip](https://gitlab.com/ptoseklukas/foundryvtt-dnd5e-lang-cs-cz/-/jobs/artifacts/master/raw/cs-CZ.zip?job=build "cs-CZ.zip") a rozbalit ho ve složce 'Data\module'.

Nezapomeň aktivovat tento modul v tvojem světě a poté zvolit český jazyk v základním nastavení.



## English

This module allows to choose the *Czech* in [FoundryVTT](http://foundryvtt.com/ "Foundry Virtual Tabletop") D&D5e module.
Includes the incomplete translation of the D&D5e module.

If you have any recommendation or if you find any errors in the translation. So please, let me know with a message to my discord **Mrkew#3758** through the FoundryVTT discord server or at my e-mail mrkew2 (at) gmail.com

### Installation

In the 'Add-On Modules' tab of the main menu, click on 'Install Module' and write down this to the pop-up window:

https://gitlab.com/ptoseklukas/foundryvtt-dnd5e-lang-cs-cz/-/raw/master/cs-CZ/module.json

If that doesn't work, you could try downloading the file [cs-CZ.zip](https://gitlab.com/ptoseklukas/foundryvtt-dnd5e-lang-cs-cz/-/jobs/artifacts/master/raw/cs-CZ.zip?job=build "cs-CZ.zip") and unzip it in the 'Data\modules' folder.

Also, you have to activate the module in your world, and then choose the language Czech from the dropdown menu in the general settings.
